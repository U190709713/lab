package generics.stack;

import java.util.Collections;
import java.util.List;

public interface Stack<T> {
    void push(T item);
    T pop();
    boolean empty();

    List<T> toList();

    default void addAll(Stack<? extends T> aStack){
        List<? extends T> values = aStack.toList(); // (? extends T) means T and every class extends from T (subclass) are accepted
        Collections.reverse(values);
        for(T value : values){
            push(value);
        }
    }
}
