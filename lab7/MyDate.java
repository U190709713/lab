public class MyDate {
    private int day , month ,year;
    int [] maxDays = {31,29,31,30,31,30,31,31,30,31,30,31};

    public MyDate(int day,int month,int year){
        this.day = day;
        this.month = month-1;
        this.year = year;
    }

    public String toString() {
        return year + "-" + ((month+1) < 10 ? "0" : "") + (month+1) + "-" + (day < 10 ? "0" : "") + day;
    }

    public void incrementDay() {
        incrementDay(1);
    }

    private boolean leapYear() {
        return year % 4 == 0 ? true : false;
    }

    public void incrementYear(int diff) {
        year += diff;
        if (month == 1 && day == 29 && !leapYear()){
            day = 28;
        }
    }

    public void decrementDay() {
        decrementDay(1);
    }

    public void decrementYear() {
        incrementYear(-1);
    }

    public void decrementMonth() {
       incrementMonth(-1);
    }

    public void incrementDay(int diff) {
        int newDay = day+diff;
        int maxDay = maxDays[month];
        if (newDay > maxDay){
            incrementMonth();
            day = 1;
        }else if (month == 1 && newDay == 29 && !leapYear()) {
            day = 1;
            incrementMonth();
        }else
            day = newDay;
    }

    public void decrementMonth(int diff) {
        incrementMonth(-diff);
    }

    public void decrementDay(int diff) {
        int newDay = day-diff;
        if (newDay == 0){
            day = 31;
            decrementMonth();
        }else{
            day = newDay;
        }
    }

    public void incrementMonth(int diff) {
        int newMonth = (month + diff) % 12;
        int yearDiff = 0;

        if(newMonth < 0){
            newMonth += 12;
            year -= 1;
        }
        yearDiff = (month + diff) / 12;
        month = newMonth;
        year += yearDiff;
        if (day > maxDays[month]){
            day = maxDays[month];
            if (month == 1 && day == 29 && !leapYear()){
                day = 28;
            }
        }
    }

    public void decrementYear(int diff) {
        incrementYear(-diff);
    }

    public void incrementMonth() {
        incrementMonth(1);
    }

    public void incrementYear() {
        incrementYear(1);
    }

    public boolean isBefore(MyDate anotherDate) {
        if (year < anotherDate.year){
            return true;
        }else if (year == anotherDate.year && month < anotherDate.month){
            return true;
        }else return year == anotherDate.year && month == anotherDate.month && day < anotherDate.day;
    }

    public boolean isAfter(MyDate anotherDate) {
        return !isBefore(anotherDate);
    }

    public int dayDifference(MyDate anotherDate) {
        int counter = 0, newDay = day, newMoth = month, newYear = year;
        do{
            if (isAfter(anotherDate)) {
                decrementDay();
            }else{
                incrementDay();
            }
            counter++;
        }while (year != anotherDate.year || month != anotherDate.month || day != anotherDate.day);
        day = newDay ; month = newMoth; year = newYear;
        return counter;
    }
}
